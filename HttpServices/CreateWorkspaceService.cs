using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class CreateWorkspaceService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<CreateWorkspaceService> logger;

    public CreateWorkspaceService(
        IHttpClientFactory factory,
        ILogger<CreateWorkspaceService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }
    
    public async Task<CreateWorkspaceResult?> Send(string apiKey, WorkspaceResult workspace)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = "/workspaces";
        var json = JsonConvert.SerializeObject(workspace, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
        var result = await client.PostAsync(route, content);
        string resultContent = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
        if (!result.IsSuccessStatusCode)
        {
            logger.LogError(
                "HTTP request was not successful \n" +
                "Route: {route} \n" +
                "Status: {status} \n" +
                "Body: {body}",
                route, result.StatusCode, resultContent
            );
            return null;
        }

        logger.LogInformation("Workspace created successfully!");

        CreateWorkspaceResult wsResult;
        try
        {
            wsResult = JsonConvert.DeserializeObject<CreateWorkspaceResult>(resultContent)!;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Error deserializing created workspace result: {body}", resultContent);
            return null;
        }
        return wsResult;
    }
}