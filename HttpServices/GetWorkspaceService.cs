using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class GetWorkspaceService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<GetWorkspaceService> logger;

    public GetWorkspaceService(
        IHttpClientFactory factory,
        ILogger<GetWorkspaceService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<WorkspaceResult?> GetWorkspace(string apiKey, string workspaceId)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = $"/workspaces/{workspaceId}";
        var result = await client.GetAsync(route);
        string content = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
        if (!result.IsSuccessStatusCode)
        {
            logger.LogError(
                "HTTP request was not successful (route {route}) \n" +
                "Status: {statusCode} \n" +
                "Body: {content}",
                route,
                result.StatusCode,
                content
            );
            return null;
        }
        WorkspaceResult deserialized;
        try
        {
            deserialized = JsonConvert.DeserializeObject<WorkspaceResult>(content)!;
        }
        catch (Exception)
        {
            logger.LogError($"Expected a {nameof(WorkspaceResult)}, but the result body could not be deserialized: \n {content}");
            return null;
        }
        return deserialized;
    }
}
