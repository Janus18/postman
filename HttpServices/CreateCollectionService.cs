
using Microsoft.Extensions.Logging;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class CreateCollectionService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<CreateCollectionService> logger;

    public CreateCollectionService(
        IHttpClientFactory factory,
        ILogger<CreateCollectionService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<bool> Send(string apiKey, Collection collection, string? workspaceId = null)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = "/collections";
        if (!string.IsNullOrEmpty(workspaceId))
        {
            route += $"?workspace={workspaceId}";
        }
        var content = new StringContent(collection.ToString(), System.Text.Encoding.UTF8, "application/json");
        var result = await client.PostAsync(route, content);
        if (!result.IsSuccessStatusCode)
        {
            var resultContent = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
            logger.LogError(
                "Error creating collection \n" +
                "Route: {route} \n" +
                "Status: {statusCode} \n" +
                "Body: {content}",
                route, result.StatusCode, resultContent
            );
            return false;
        }
        logger.LogInformation("Collection created successfully!");
        return true;
    }
}
