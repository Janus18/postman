using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class GetEnvironmentService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<GetEnvironmentService> logger;

    public GetEnvironmentService(
        IHttpClientFactory factory,
        ILogger<GetEnvironmentService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<EnvironmentResult?> Send(string apiKey, string envId)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = $"/environments/{envId}";
        var result = await client.GetAsync(route);
        string content = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
        if (!result.IsSuccessStatusCode)
        {
            logger.LogError(
                "HTTP request was not successful \n" +
                "Route {route} \n" +
                "Status {status} \n" +
                "Body {body}",
                route,
                result.StatusCode,
                content
            );
            return null;
        }
        EnvironmentResult env;
        try
        {
            env = JsonConvert.DeserializeObject<EnvironmentResult>(content)!;
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Could not deserialize environment result \n Route {route} \n Body {body}", route, content);
            return null;
        }
        return env;
    }
}
