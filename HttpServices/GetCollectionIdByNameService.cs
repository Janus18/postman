using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Postman.Extensions;

namespace Postman.HttpServices;

public class GetCollectionIdByNameService
{
    private readonly IHttpClientFactory factory;

    public GetCollectionIdByNameService(
        IHttpClientFactory factory
    )
    {
        this.factory = factory;
    }

    public async Task<string> Send(string apiKey, string collectionName)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = "/collections";
        var result = await client.GetAsync(route);
        string content = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
        if (!result.IsSuccessStatusCode)
        {
            throw new HttpRequestException(
                $"Request was not successful ({route}) \n" +
                $"Status: {result.StatusCode} \n" +
                $"Body: {content}"
            );
        }
        else
        {
            var json = JsonConvert.DeserializeObject<JToken>(content);
            if (json == null)
                throw new JsonException($"The response ({route}) didn't contain a JSON object: \n {content}");
            var collections = json.SelectTokens($"$.collections[?(@.name == '{collectionName}')].id");
            if (!collections.Any())
                throw new HttpRequestException($"No collection with name \"{collectionName}\" found for ApiKey {apiKey}");
            return collections.First().Value<string>()!;
        }
    }
}
