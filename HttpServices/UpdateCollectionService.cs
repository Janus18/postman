using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class UpdateCollectionService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<UpdateCollectionService> logger;

    public UpdateCollectionService(
        IHttpClientFactory factory,
        ILogger<UpdateCollectionService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<bool> Send(string apiKey, string collectionId, Collection collection)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        var content = new StringContent(collection.ToString(), System.Text.Encoding.UTF8, "application/json");
        var result = await client.PutAsync($"collections/{collectionId}", content);
        if (!result.IsSuccessStatusCode)
        {
            var resultContent = result.Content != null ? await result.Content.ReadAsStringAsync() : null;
            logger.LogError(
                "Put request failed \n" +
                "Status code: {statusCode} \n" +
                "Body: {content}",
                result.StatusCode,
                resultContent
            );
            return false;
        }
        logger.LogInformation("Collection updated successfully!");
        return true;
    }
}
