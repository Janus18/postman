using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class CreateEnvironmentService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<CreateEnvironmentService> logger;

    public CreateEnvironmentService(
        IHttpClientFactory factory,
        ILogger<CreateEnvironmentService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<bool> Send(string apiKey, EnvironmentResult env, string? workspaceId = null)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        string route = "/environments";
        if (!string.IsNullOrEmpty(workspaceId))
        {
            route += $"?workspace={workspaceId}";
        }
        string json = JsonConvert.SerializeObject(env);
        var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
        var result = await client.PostAsync(route, content);
        if (!result.IsSuccessStatusCode)
        {
            string resultContent = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
            logger.LogError(
                "HTTP request was not successful \n" +
                "Route: {route} \n" +
                "Status: {status} \n" +
                "Body: {body}",
                route, result.StatusCode, resultContent
            );
            return false;
        }
        logger.LogInformation("Environment created successfully!");
        return true;
    }
}