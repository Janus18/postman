using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class UpdateEnvironmentService
{
    private readonly IHttpClientFactory factory;
    private readonly ILogger<UpdateEnvironmentService> logger;

    public UpdateEnvironmentService(
        IHttpClientFactory factory,
        ILogger<UpdateEnvironmentService> logger
    )
    {
        this.factory = factory;
        this.logger = logger;
    }

    public async Task<bool> Send(string apiKey, string envId, EnvironmentResult environment)
    {
        var client = factory.WithApiKey(apiKey, Dependencies.PostmanClientName);
        var json = JsonConvert.SerializeObject(environment);
        var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
        string route = $"/environments/{envId}";
        var result = await client.PutAsync(route, content);
        if (!result.IsSuccessStatusCode)
        {
            string resultContent = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
            logger.LogError(
                "Error updating environment (route {route}) \n" +
                "Status code: {status} \n" +
                "Body: {body}",
                route,
                result.StatusCode,
                resultContent
            );
            return false;
        }
        return true;
    }
}
