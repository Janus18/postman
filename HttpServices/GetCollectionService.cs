using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Postman.Extensions;
using Postman.Models;

namespace Postman.HttpServices;

public class GetCollectionService
{
    private readonly IHttpClientFactory httpClientFactory;
    private readonly ILogger<GetCollectionService> logger;

    public GetCollectionService(
        IHttpClientFactory httpClientFactory,
        ILogger<GetCollectionService> logger
    )
    {
        this.httpClientFactory = httpClientFactory;
        this.logger = logger;
    }

    public async Task<Collection?> Send(string apiKey, string collectionId)
    {
        var client = httpClientFactory.WithApiKey(apiKey!, Dependencies.PostmanClientName);
        string route = $"/collections/{collectionId}";
        var result = await client.GetAsync(route);
        string content = result.Content != null ? await result.Content.ReadAsStringAsync() : "";
        if (!result.IsSuccessStatusCode)
        {
            logger.LogError(
                "Request was not successful ({route}) \n" +
                "Status: {statusCode} \n" +
                "Body: {content}",
                route,
                result.StatusCode,
                content
            );
            return null;
        }
        else
        {
            JToken json;
            try
            {
                json = JsonConvert.DeserializeObject<JToken>(content)!;
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error deserializing collection \n Route: {route}, \n Body: {body}", route, content);
                return null;
            }
            return new Collection(json);
        }
    }
}
