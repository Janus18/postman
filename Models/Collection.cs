using Newtonsoft.Json.Linq;
using Postman.Extensions;

namespace Postman.Models;

/// <summary>
/// <para>Postman collection.</para>
/// <para>
/// The collection model is large, it is not worth having it all here,
/// so this is just a wrapper around a json value.
/// </para>
/// </summary>
public class Collection
{
    public JToken Value { get; set; }

    public Collection(JToken value)
    {
        Value = value;
    }

    /// <summary>
    /// Returns a new collection model ready to be used for an update.
    /// </summary>
    /// <param name="token"></param>
    /// <returns>New Collection ready for update</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public Collection PrepareForSync()
    {
        var clone = Value.DeepClone();
        clone.TryRemove("$.collection.info._postman_id");
        clone.TryRemove("$..id");
        return new Collection(clone);
    }

    /// <summary>
    /// Returns the name of the collection if it's present.
    /// </summary>
    /// <returns>String representing the name of the Collection</returns>
    public string? Name()
    {
        var nameToken = Value.SelectToken("$.collection.info.name");
        return nameToken?.Value<string>();
    }

    /// <summary>
    /// Returns a new Collection, cloned from this Collection except for the name.
    /// </summary>
    /// <param name="collectionName"></param>
    /// <returns>New Collection with given name</returns>
    public Collection Rename(string collectionName)
    {
        var clone = Value.DeepClone();
        var nameToken = clone.SelectToken("$.collection.info.name");
        if (nameToken != null)
        {
            clone["collection"]!["info"]!["name"] = collectionName;
        }
        return new Collection(clone);
    }

    public override string ToString() => Value.ToString();
}
