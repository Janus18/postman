using Newtonsoft.Json;

namespace Postman.Models;

public class WorkspaceResultId
{
    public WorkspaceResult Workspace { get; }
    
    public string Id { get; }

    public WorkspaceResultId(string id, WorkspaceResult workspace)
    {
        Id = id;
        Workspace = workspace;
    }
}

public class WorkspaceResult
{
    [JsonProperty("workspace")]
    public WorkspaceData? Workspace { get; set; }
}

public class WorkspaceData
{
    [JsonProperty("name")]
    public string Name { get; set; } = "";

    [JsonProperty("type")]
    public string Type { get; set; } = "";

    [JsonProperty("description")]
    public string Description { get; set; } = "";

    [JsonProperty("collections")]
    public IList<WorkspaceCollection>? Collections { get; set; }

    [JsonProperty("environments")]
    public IList<WorkspaceEnvironment>? Environments { get; set; }
}

public class WorkspaceEnvironment
{
    [JsonProperty("id")]
    public string Id { get; set; } = "";

    [JsonProperty("name")]
    public string Name { get; set; } = "";

    [JsonProperty("uid")]
    public string Uid { get; set; } = "";
}

public class WorkspaceCollection
{
    [JsonProperty("id")]
    public string Id { get; set; } = "";

    [JsonProperty("name")]
    public string Name { get; set; } = "";

    [JsonProperty("uid")]
    public string Uid { get; set; } = "";
}