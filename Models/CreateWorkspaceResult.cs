using Newtonsoft.Json;

namespace Postman.Models;

public class CreateWorkspaceResult
{
    [JsonProperty("workspace")]
    public CreateWorkspaceResultWorkspace? Workspace { get; set; }
}

public class CreateWorkspaceResultWorkspace
{
    [JsonProperty("id")]
    public string Id { get; set; } = "";
}