
namespace Postman.Models;

public class ValidationResult
{
    public bool IsValid { get; }

    public string? Reason { get; }

    public ValidationResult(bool isValid, string? reason)
    {
        IsValid = isValid;
        Reason = reason;
    }
}