namespace Postman.Models;

public class CollectionIdentifier
{
    public string ApiKey { get; }

    public string? CollectionId { get; }

    public string? CollectionName { get; }

    public bool HasId => !string.IsNullOrEmpty(CollectionId);

    public CollectionIdentifier(
        string apiKey,
        string? collectionId = null,
        string? collectionName = null
    )
    {
        if (string.IsNullOrEmpty(collectionId) || string.IsNullOrEmpty(collectionName))
            throw new ArgumentException("You must indicate either the collection's Id or its name");
        ApiKey = apiKey;
        CollectionId = collectionId;
        CollectionName = collectionName;
    }
}
