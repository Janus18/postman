using Newtonsoft.Json;

namespace Postman.Models;

public class EnvironmentResult
{
    [JsonProperty("environment")]
    public PostmanEnvironment? Environment { get; set; }
}

public class PostmanEnvironment
{
    [JsonProperty("name")]
    public string Name { get; set; } = "";

    [JsonProperty("values")]
    public PostmanEnvironmentValue[]? Values { get; set; }
}

public class PostmanEnvironmentValue
{
    [JsonProperty("key")]
    public string Key { get; set; } = "";

    [JsonProperty("value")]
    public string Value { get; set; } = "";
}