
namespace Postman.Models;

public class WorkspaceElements
{
    public WorkspaceResult Workspace { get; }
    
    public IDictionary<string, Collection> Collections { get; }

    public IDictionary<string, EnvironmentResult> Environments { get; }

    public WorkspaceElements(
        WorkspaceResult workspace,
        IDictionary<string, Collection> collections,
        IDictionary<string, EnvironmentResult> environments
    )
    {
        Workspace = workspace;
        Collections = collections;
        Environments = environments;
    }
}