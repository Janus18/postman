using Newtonsoft.Json.Linq;

namespace Postman.Extensions;

public static class JTokenExtensions
{
    public static void TryRemove(this JToken token, string path)
    {
        var tokens = token.SelectTokens(path).ToList();
        foreach (var tkn in tokens)
        {
            if (tkn.Parent is not null)
                tkn.Parent!.Remove();
        }
    }
}
