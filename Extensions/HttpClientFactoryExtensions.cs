namespace Postman.Extensions;

public static class HttpClientFactoryExtensions
{
    public static HttpClient WithApiKey(this IHttpClientFactory factory, string apiKey, string? clientName = null)
    {
        var client = string.IsNullOrEmpty(clientName)
            ? factory.CreateClient()
            : factory.CreateClient(clientName);
        client.DefaultRequestHeaders.Add("X-API-Key", apiKey);
        return client;
    }
}
