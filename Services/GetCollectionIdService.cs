using Postman.HttpServices;
using Postman.Models;

namespace Postman.Services;

public class GetCollectionIdService
{
    private readonly GetCollectionIdByNameService getCollectionIdByNameService;

    public GetCollectionIdService(
        GetCollectionIdByNameService getCollectionIdByNameService
    )
    {
        this.getCollectionIdByNameService = getCollectionIdByNameService;
    }

    public async Task<string> GetCollectionId(CollectionIdentifier identifier)
    {
        if (identifier.HasId)
            return identifier.CollectionId!;
        return await getCollectionIdByNameService.Send(identifier.ApiKey, identifier.CollectionName!);
    }
}
