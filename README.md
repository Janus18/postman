# Postman utility
The purpose of this project is to share one of the user's workspaces with other people.  
You can create a private Postman workspace for your team, which will sync all your work, but the free tier only allows three members in it. If you can't afford it right now, with this utility you can share that workspace with other team members, and easily keep them updated.  

### Usage
This is a console application, you can run it with **dotnet run**. Without arguments, the output will be a help message with all the available commands and a short description of each. The main command you'll be using is **SyncWorkspace**.
#### Configuration
The configuration indicating what will be updated must be written on the appsettings file or dotnet user secrets. This is so it's easier to run the command frequently. The format of the settings is the following:
```
{
    "SyncData":
    {
        "Source":
        {
            "ApiKey": "SourceApiKey",
            "WorkspaceId": "SourceWorkspaceId"
        },
        "Targets":
        [
            {
                "ApiKey": "Target1ApiKey",
                "WorkspaceId": "Target1WorkspaceId"
            },
            {
                "ApiKey": Target2ApiKey",
                "WorkspaceId": "Target2WorkspaceId"
            }
        ]
    }
}
```
These settings indicate the application to update two targets' workspaces with the collections and environments from the source workspace.  
You can obtain the API keys from each user's profile. In case a workspace belongs to a team, you can use any of the team member's API keys to access it.  
#### What is synchronized
The synchronization works only on collections and environments, no other attributes or elements will be updated nor deleted. Each collection will be synchronized in the following way (the same is true for environments):  
* If the target workspace contains a collection with the same name as the source collection, that target collection will be totally replaced with the source collection. The target user should be aware that any changes they make to the collection will be lost.
* If the target workspace does not contain a collection with the same name, the source collection will be recreated in the target workspace.
* All collections in the target workspace which are not in the source workspace will not be updated nor deleted.

#### Bootstrap
The first time you synchronize with a new user, you'll probably want to create a new target workspace in their profile. You can indicate them to create it manually, and they'll handle you the workspace ID to paste in your config. Or you can just execute the *SyncWorkspace* command leaving the target's *WorkspaceId* empty. This indicates the application to create a new private workspace for that user, with the same name as the source workspace, and clone all collections and environments into it. After doing so, you can search the logs for that workspace's ID, and paste it into the settings to keep it in sync from now on.
