using Postman.Models;
using PowerArgs;

namespace Postman.Commands;

public class GetCollectionCommand : ICommand
{
    [ArgShortcut("key")]
    [ArgDescription("API key for the owner of the collection")]
    public string? ApiKey { get; set; }

    [ArgShortcut("id")]
    [ArgDescription("Collection UUID, found under collection's info")]
    public string? CollectionId { get; set; }

    [ArgShortcut("name")]
    [ArgDescription("Collection name")]
    public string? CollectionName { get; set; }

    [ArgShortcut("o")]
    [ArgDescription("Saves the result to this file if given, otherwise it's written on console")]
    public string? Output { get; set; }

    public CollectionIdentifier GetIdentifier()
    {
        AssertIsComplete();
        return new CollectionIdentifier(ApiKey!, CollectionId, CollectionName);
    }

    private void AssertIsComplete()
    {
        if (string.IsNullOrEmpty(ApiKey))
            throw new ArgumentException($"The {nameof(ApiKey)} is required");
        if (string.IsNullOrEmpty(CollectionId) && string.IsNullOrEmpty(CollectionName))
            throw new ArgumentException($"You must supply either the {nameof(CollectionId)} or the {nameof(CollectionName)}");
    }
}
