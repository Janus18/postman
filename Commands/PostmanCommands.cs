using Postman.Handlers;
using PowerArgs;

namespace Postman.Commands;

[ArgExceptionBehavior(ArgExceptionPolicy.StandardExceptionHandling)]
public class PostmanCommands
{
    private readonly Dependencies deps = new Dependencies();

    [ArgActionMethod, ArgDescription("Obtains a collection by Id or name.")]
    public void GetCollection(GetCollectionCommand command)
    {
        Execute(command);
    }

    [ArgActionMethod, ArgDescription(
        "Synchronize all target workspaces with the source workspace.\n" +
        "Arguments for the command are obtained from the app settings or user secrets."
    )]
    public void SyncWorkspace(SyncWorkspaceCommand command)
    {
        Execute(command);
    }

    [ArgActionMethod, ArgDescription("Prints the current app settings to the console for debug purposes.")]
    public void PrintSettings(PrintSettingsCommand command)
    {
        Execute(command);
    }

    private void Execute<T>(T command) where T : ICommand
    {
        var handler = deps.GetService<IHandler<T>>();
        if (handler == null)
            throw new ApplicationException($"No handler registered for command {typeof(T).Name}");
        handler.Execute(command).Wait();
    }
}