using Postman.Commands;

namespace Postman.Handlers;

public interface IHandler<TCommand> where TCommand : ICommand
{
    Task Execute(TCommand cmd);
}
