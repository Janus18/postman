using Microsoft.Extensions.Logging;
using Postman.HttpServices;
using Postman.Models;

namespace Postman.Handlers;

public abstract class WorkspaceHelper
{
    private readonly GetCollectionService getCollectionService;
    private readonly GetEnvironmentService getEnvironmentService;
    private readonly ILogger<WorkspaceHelper> logger;

    public WorkspaceHelper(
        GetCollectionService getCollectionService,
        GetEnvironmentService getEnvironmentService,
        ILogger<WorkspaceHelper> logger
    )
    {
        this.getCollectionService = getCollectionService;
        this.getEnvironmentService = getEnvironmentService;
        this.logger = logger;
    }

    protected async Task<IDictionary<string, Collection>> GetWorkspaceCollections(string apiKey, WorkspaceResult workspace)
    {
        var collections = await FindWorkspaceCollections(apiKey, workspace);
        return MapToSyncCollections(collections);
    }

    protected async Task<IDictionary<string, EnvironmentResult>> GetWorkspaceEnvironments(string apiKey, WorkspaceResult workspace)
    {
        var environments = await FindWorkspaceEnvironments(apiKey, workspace);
        return environments.ToDictionary(env => env.Environment!.Name);
    }

    private async Task<IList<Collection>> FindWorkspaceCollections(string apiKey, WorkspaceResult workspace)
    {
        var workspaceCollectionsIds = workspace?.Workspace?.Collections?.Select(c => c.Uid).ToList() ?? new List<string>();
        if (!workspaceCollectionsIds.Any())
        {
            logger.LogInformation("No collections found on source workspace");
            return new List<Collection>();
        }
        var tasks = workspaceCollectionsIds.Select(id => getCollectionService.Send(apiKey, id));
        var results = await Task.WhenAll(tasks);
        return results.Where(r => r != null).Select(r => r!).ToList();
    }

    private async Task<IList<EnvironmentResult>> FindWorkspaceEnvironments(string apiKey, WorkspaceResult workspace)
    {
        var workspaceEnvironmentsIds = workspace?.Workspace?.Environments?.Select(c => c.Uid).ToList() ?? new List<string>();
        if (!workspaceEnvironmentsIds.Any())
        {
            logger.LogInformation("No environments found on source workspace");
            return new List<EnvironmentResult>();
        }
        var tasks = workspaceEnvironmentsIds.Select(id => getEnvironmentService.Send(apiKey, id));
        var results = await Task.WhenAll(tasks);
        return results.Where(r => r != null).Select(r => r!).ToList();
    }

    private IDictionary<string, Collection> MapToSyncCollections(IList<Collection> collections)
    {
        var result = new Dictionary<string, Collection>();
        foreach (var collection in collections)
        {
            string? name = collection.Name();
            if (string.IsNullOrEmpty(name))
            {
                logger.LogWarning("Found collection with no name: {collection}", collection.ToString());
                continue;
            }
            result.Add(name, collection.PrepareForSync());
        }
        return result;
    }
}
