using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Postman.Commands;
using Postman.HttpServices;
using Postman.Models;

namespace Postman.Handlers;

public class SyncWorkspaceHandler : WorkspaceHelper, IHandler<SyncWorkspaceCommand>
{
    private readonly GetWorkspaceService getWorkspaceService;
    private readonly UpdateCollectionService updateCollectionService;
    private readonly UpdateEnvironmentService updateEnvironmentService;
    private readonly CreateCollectionService createCollectionService;
    private readonly CreateEnvironmentService createEnvironmentService;
    private readonly CreateWorkspaceService createWorkspaceService;
    private readonly ILogger<SyncWorkspaceHandler> logger;
    private readonly Settings settings;

    public SyncWorkspaceHandler(
        GetWorkspaceService getWorkspaceService,
        GetCollectionService getCollectionService,
        GetEnvironmentService getEnvironmentService,
        UpdateCollectionService updateCollectionService,
        UpdateEnvironmentService updateEnvironmentService,
        CreateCollectionService createCollectionService,
        CreateEnvironmentService createEnvironmentService,
        CreateWorkspaceService createWorkspaceService,
        ILogger<SyncWorkspaceHandler> logger,
        IOptions<Settings> options
    ) : base(getCollectionService, getEnvironmentService, logger)
    {
        this.getWorkspaceService = getWorkspaceService;
        this.updateCollectionService = updateCollectionService;
        this.updateEnvironmentService = updateEnvironmentService;
        this.createCollectionService = createCollectionService;
        this.createEnvironmentService = createEnvironmentService;
        this.createWorkspaceService = createWorkspaceService;
        this.logger = logger;
        settings = options.Value;
    }

    public async Task Execute(SyncWorkspaceCommand cmd)
    {
        var syncValidationResult = settings.SyncIsPossible();
        if (!syncValidationResult.IsValid)
        {
            logger.LogInformation(syncValidationResult.Reason);
            logger.LogInformation("Aborting operation");
            return;
        }
        var sourceWorkspace = await GetSourceWorkspace(settings.SyncData!.Source!);
        foreach (var target in settings.SyncData.Targets!)
        {
            await SyncWorkspace(sourceWorkspace, target);
        }
    }

    private async Task<WorkspaceElements> GetSourceWorkspace(WorkspaceData sourceWorkspaceData)
    {
        var workspace = await getWorkspaceService.GetWorkspace(sourceWorkspaceData!.ApiKey, sourceWorkspaceData.WorkspaceId)
            ?? throw new NullReferenceException();
        var sourceCollections = await GetWorkspaceCollections(sourceWorkspaceData.ApiKey, workspace);
        var sourceEnvironments = await GetWorkspaceEnvironments(sourceWorkspaceData.ApiKey, workspace);
        return new WorkspaceElements(workspace, sourceCollections, sourceEnvironments);
    }

    private async Task SyncWorkspace(
        WorkspaceElements sourceWorkspace,
        WorkspaceData targetWorkspaceData
    )
    {
        WorkspaceResultId? targetWorkspace = await GetTargetWorkspace(sourceWorkspace.Workspace, targetWorkspaceData);
        if (targetWorkspace == null)
        {
            logger.LogError("Target is null, skipping target {Target}", targetWorkspaceData.ToString());
            return;
        }
        foreach (var (name, collection) in sourceWorkspace.Collections)
        {
            await SyncCollection(
                new WorkspaceData { ApiKey = targetWorkspaceData.ApiKey, WorkspaceId = targetWorkspace.Id },
                targetWorkspace.Workspace?.Workspace?.Collections?.FirstOrDefault(c => c.Name == name),
                collection);
        }

        foreach (var (name, environment) in sourceWorkspace.Environments)
        {
            await SyncEnvironment(
                new WorkspaceData { ApiKey = targetWorkspaceData.ApiKey, WorkspaceId = targetWorkspace.Id },
                targetWorkspace.Workspace?.Workspace?.Environments?.FirstOrDefault(env => env.Name == name),
                environment);
        }
    }

    private async Task<WorkspaceResultId?> GetTargetWorkspace(WorkspaceResult sourceWorkspace, WorkspaceData targetWorkspaceData)
    {
        string? workspaceId = targetWorkspaceData.WorkspaceId;
        if (string.IsNullOrEmpty(workspaceId))
        {
            logger.LogInformation("Target Workspace ID was not specified, a new workspace for {ApiKey} will be created.", targetWorkspaceData.ApiKey);
            var newWorkspace = new WorkspaceResult
            {
                Workspace = new Models.WorkspaceData
                {
                    Name = sourceWorkspace.Workspace!.Name,
                    Type = "personal",
                    Description = sourceWorkspace.Workspace?.Description ?? "Description"
                }
            };
            var createdWorkspace = await createWorkspaceService.Send(targetWorkspaceData.ApiKey, newWorkspace);
            if (string.IsNullOrEmpty(createdWorkspace?.Workspace?.Id))
            {
                logger.LogError("Could not create new workspace for {ApiKey}.", targetWorkspaceData.ApiKey);
                return null;
            }
            workspaceId = createdWorkspace!.Workspace!.Id;
            logger.LogInformation("Workspace created successfully. The new ID is {ID}.", workspaceId);
        }
        var targetWorkspace = await getWorkspaceService.GetWorkspace(targetWorkspaceData.ApiKey, workspaceId);
        return targetWorkspace is null ? null : new WorkspaceResultId(workspaceId, targetWorkspace);
    }

    private async Task SyncCollection(WorkspaceData targetWorkspaceData, WorkspaceCollection? targetCollection, Collection collection)
    {
        if (targetCollection == null)
        {
            logger.LogInformation(
                "Target {target} has no collection with name {collectionName}, it will be created",
                targetWorkspaceData.ToString(), collection.Name());
            await createCollectionService.Send(targetWorkspaceData.ApiKey, collection, targetWorkspaceData.WorkspaceId);
        }
        else
        {
            logger.LogInformation(
                "Updating collection {collectionName} on target {target}",
                targetCollection.Name, targetWorkspaceData.ToString());
            await updateCollectionService.Send(targetWorkspaceData.ApiKey, targetCollection.Id, collection);
        }
    }

    private async Task SyncEnvironment(WorkspaceData targetWorkspaceData, WorkspaceEnvironment? targetEnvironment, EnvironmentResult sourceEnvironment)
    {
        if (targetEnvironment == null)
        {
            logger.LogInformation(
                "Target {target} has no environment with name {environmentName}, it will be created",
                targetWorkspaceData.ToString(), sourceEnvironment.Environment!.Name);
            await createEnvironmentService.Send(targetWorkspaceData.ApiKey, sourceEnvironment, targetWorkspaceData.WorkspaceId);
        }
        else
        {
            logger.LogInformation(
                "Updating environment {environmentName} on target {target}",
                targetEnvironment.Name, targetWorkspaceData.ToString());
            await updateEnvironmentService.Send(targetWorkspaceData.ApiKey, targetEnvironment.Id, sourceEnvironment);
        }
    }
}
