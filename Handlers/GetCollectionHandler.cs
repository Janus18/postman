using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Postman.Commands;
using Postman.HttpServices;
using Postman.Models;
using Postman.Services;

namespace Postman.Handlers;

public class GetCollectionHandler : IHandler<GetCollectionCommand>
{
    private readonly GetCollectionService getCollectionService;
    private readonly GetCollectionIdService getCollectionIdService;
    private readonly Settings settings;

    public GetCollectionHandler(
        GetCollectionService getCollectionService,
        GetCollectionIdService getCollectionIdService,
        IOptions<Settings> options
    )
    {
        this.getCollectionService = getCollectionService;
        this.getCollectionIdService = getCollectionIdService;
        settings = options.Value;
    }

    public async Task Execute(GetCollectionCommand cmd)
    {
        var collectionIdentifier = cmd.GetIdentifier();
        var collectionId = await getCollectionIdService.GetCollectionId(collectionIdentifier);
        var json = await getCollectionService.Send(cmd.ApiKey!, collectionId);
        if (json == null)
            return;
        await OutputResult(cmd, json);
    }

    private static async Task OutputResult(GetCollectionCommand cmd, Collection result)
    {
        var formatted = JsonConvert.SerializeObject(result, Formatting.Indented);
        if (!string.IsNullOrEmpty(cmd.Output))
        {
            await System.IO.File.WriteAllTextAsync(cmd.Output, formatted);
        }
        else
        {
            Console.WriteLine(formatted);
        }
    }
}
