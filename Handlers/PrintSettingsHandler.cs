using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Postman.Commands;

namespace Postman.Handlers;

public class PrintSettingsHandler : IHandler<PrintSettingsCommand>
{
    private readonly Settings settings;

    public PrintSettingsHandler(IOptions<Settings> options)
    {
        settings = options.Value;
    }

    public Task Execute(PrintSettingsCommand cmd)
    {
        var formatted = JsonConvert.SerializeObject(settings, Formatting.Indented);
        Console.WriteLine(formatted);
        return Task.CompletedTask;
    }
}