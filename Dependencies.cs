using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Postman.Commands;
using Postman.Handlers;
using Postman.HttpServices;
using Postman.Services;

namespace Postman;

public class Dependencies : IDisposable
{
    private readonly ServiceProvider _services;

    public const string PostmanClientName = "Postman";

    public Dependencies()
    {
        var config = new ConfigurationBuilder()
            .AddUserSecrets<Dependencies>()
            .AddJsonFile("settings.json", true)
            .Build();
        var settings = new Settings();
        config.Bind(settings);

        var services = new ServiceCollection();
        services.Configure<Settings>(config);
        services.AddHttpClient(
            PostmanClientName,
            c =>
            {
                c.BaseAddress = new Uri("https://api.getpostman.com");
            }
        );
        services.AddLogging(l => l.AddConsole().SetMinimumLevel(LogLevel.Debug));

        services.AddSingleton<CreateWorkspaceService>();
        services.AddSingleton<CreateEnvironmentService>();
        services.AddSingleton<CreateCollectionService>();
        services.AddSingleton<UpdateCollectionService>();
        services.AddSingleton<UpdateEnvironmentService>();
        services.AddSingleton<GetCollectionService>();
        services.AddSingleton<GetCollectionIdByNameService>();
        services.AddSingleton<GetCollectionIdService>();
        services.AddSingleton<GetWorkspaceService>();
        services.AddSingleton<GetEnvironmentService>();
        services.AddSingleton<IHandler<GetCollectionCommand>, GetCollectionHandler>();
        services.AddSingleton<IHandler<SyncWorkspaceCommand>, SyncWorkspaceHandler>();
        services.AddSingleton<IHandler<PrintSettingsCommand>, PrintSettingsHandler>();
        _services = services.BuildServiceProvider();
    }

    public T? TryGetService<T>() => _services.GetService<T>();

    public T GetService<T>() => _services.GetService<T>()!;

    public void Dispose()
    {
        _services.Dispose();
        GC.SuppressFinalize(this);
    }
}
