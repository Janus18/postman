using Postman.Models;

namespace Postman;

public class Settings
{
    public WorkspaceSyncData? SyncData { get; set; }

    public ValidationResult SyncIsPossible()
    {
        if (SyncData == null)
        {
            return new ValidationResult(false, "No sync data found in the application settings.");
        }
        if (SyncData.Source == null)
        {
            return new ValidationResult(false, "Source workspace data is null in the application settings.");
        }
        if (SyncData.Targets?.Any() != true)
        {
            return new ValidationResult(false, "No targets workspaces in the application settings to sync.");
        }
        return new ValidationResult(true, null);
    }
}

public class WorkspaceSyncData
{
    public WorkspaceData? Source { get; set; }

    public WorkspaceData[]? Targets { get; set; }

    public override string ToString()
    {
        return Source?.ToString() ?? nameof(WorkspaceSyncData);
    }
}

public class WorkspaceData
{
    public string ApiKey { get; set; } = "";

    public string WorkspaceId { get; set; } = "";

    public override string ToString()
    {
        return $"Workspace: {{ ApiKey: {ApiKey}, WorkspaceId: {WorkspaceId} }}";
    }
}

